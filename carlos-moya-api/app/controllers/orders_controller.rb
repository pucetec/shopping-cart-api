class OrdersController < ApplicationController
  before_action :set_headers
  def index
    render json: Order.all
  end 


  def create
    @customer = Customer.find_by_email(params[:email])
    if @customer.nil?
      render json: { message: "User not found" }
    else
      order = Order.new
      order.customer_id = @customer.id
      order.save
      total=0
      data = params["items"]
      titles = data["title"].to_unsafe_h.values
      quantities = data["quantity"].to_unsafe_h.values
      items = titles.map.with_index do |title, index|
        { title: title, quantity: quantities[index] }
      end
      items.each do |item|
        found_item = Item.find_by_title(item[:title])
        if found_item
          line_item = LineItem.new
          line_item.item_id = found_item.id
          line_item.order_id =  order.id
          line_item.quantity = item[:quantity]
          line_item.unit_price = found_item.lower_price
          line_item.save
          total = total + line_item.quantity * line_item.unit_price
        end
      end
      order.total = total
      order.save
      render :json => {:customer => @customer, :total => total, :items => items}
    end
  end
end
