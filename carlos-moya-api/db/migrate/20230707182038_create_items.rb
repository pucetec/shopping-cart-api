class CreateItems < ActiveRecord::Migration[7.0]
  def change
    create_table :items do |t|
      t.string :title
      t.decimal :lower_price, precision: 10, scale: 2
      t.text :desctiption
      t.text :src_image
      t.timestamps
    end
  end
end
