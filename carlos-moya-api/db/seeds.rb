# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

i=Item.find_or_create_by(title:"Apple EarBods")
i.lower_price= 120
i.desctiption= "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Nam facilisis fringilla nunc sit amet cursus. Donec augue felis, euismod a odio auctor, bibendum vulputate nulla."
i.src_image="https://new.axilthemes.com/demo/template/etrade/assets/images/product/electric/product-08.png"
i.save

i=Item.find_or_create_by(title:"Dual shock PS3")
i.lower_price= 80
i.desctiption= "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Nam facilisis fringilla nunc sit amet cursus. Donec augue felis, euismod a odio auctor, bibendum vulputate nulla."
i.src_image="https://new.axilthemes.com/demo/template/etrade/assets/images/product/product-38.png"
i.save

i=Item.find_or_create_by(title:"Gaming Keyboard")
i.lower_price= 75
i.desctiption= "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Nam facilisis fringilla nunc sit amet cursus. Donec augue felis, euismod a odio auctor, bibendum vulputate nulla."
i.src_image="https://new.axilthemes.com/demo/template/etrade/assets/images/product/electric/product-02.png"
i.save

i=Item.find_or_create_by(title:"Alexa V3")
i.lower_price= 500
i.desctiption= "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Nam facilisis fringilla nunc sit amet cursus. Donec augue felis, euismod a odio auctor, bibendum vulputate nulla."
i.src_image="https://new.axilthemes.com/demo/template/etrade/assets/images/product/electric/product-05.png"
i.save

i=Item.find_or_create_by(title:"Apple SmartWatch")
i.lower_price= 150
i.desctiption= "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Nam facilisis fringilla nunc sit amet cursus. Donec augue felis, euismod a odio auctor, bibendum vulputate nulla."
i.src_image="https://new.axilthemes.com/demo/template/etrade/assets/images/product/product-39.png"
i.save


c=Customer.find_or_create_by(first_name: "Nicole")
c.last_name= "Gavilanes"
c.email= "nicole.gavilanes94@gmail.com"
c.save

c=Customer.find_or_create_by(first_name: "Carlos")
c.last_name= "Moya"
c.email= "carlosmoya@gmail.com"
c.save

c=Customer.find_or_create_by(first_name: "Fabian")
c.last_name= "Rodriguez"
c.email= "frodriguez@gmail.com"
c.save

ca=CustomerAddress.find_or_create_by(streets: "12 de Octubre", city: "Quito")
ca.customer_id=1
ca.save

ca=CustomerAddress.find_or_create_by(streets: "Av. 6 de diciembre", city: "Quito")
ca.customer_id=2
ca.save

ca=CustomerAddress.find_or_create_by(streets: "Av. 10 de agosto", city: "Quito")
ca.customer_id=3
ca.save

ca=CustomerAddress.find_or_create_by(streets: "Av. Quito", city: "Manta")
ca.customer_id=4
ca.save

ca=CustomerAddress.find_or_create_by(streets: "Av. Brasil", city: "Quito")
ca.customer_id=5
ca.save



