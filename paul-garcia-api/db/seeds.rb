# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

items = [
    {name:'DELL',description:'Laptop dell inspiron, 1tb SSD storage, 8gb RAM, Intel core I7 12 gen',price:400,
    image:'https://digitalserver.com.ec/wp-content/uploads/2023/03/581202303231241000313.jpg',
    quantity:10},
    {name:'Acer',description:'Laptop Acer, SSD 250gb, 4gb RAM, Intel core i3 11gen',price:250,
    image:'https://images.acer.com/is/image/acer/travelmate-p2-p40-52_01a-1?$Product-Cards-XL$',
    quantity:10},
    {name:'Asus',description:'Laptop Asus, 500gb SSD storage, 12gb RAM, Intel core i5 11gen',price:600,
    image:'https://www.kartyy.com/16719-large_default/laptop-asus-x415ea-eb635-14-fhd-core-i3-1115g4-30ghz-11va-gen-8gb-ram-256gb-ssd-m2-nvme-teclado-espanol.jpg',
    quantity:10},
    {name:'Lenovo',description:'Laptop Lenovo, 1tb SSD storage, 16gb RAM, Intel core i5 10gen',price:700,
    image:'https://www.lenovo.com/medias/lenovo-laptop-v310-14-back.png?context=bWFzdGVyfGltYWdlc3wxMzY0NnxpbWFnZS9wbmd8aW1hZ2VzL2gwMC9oYzkvOTM0NzAyOTEzOTQ4Ni5wbmd8YTU2NTdhMjI1ODkzNTlkYjg3NGEwNzVlY2ZjNzFhYTZlNmVmYzNlYjNiZmIzMWY0MmU2NzM2OWY3ZDhjOTAwNw',
    quantity:10},
    {name:'MSI',description:'Laptop MSI, 1tb SSD storage, 16gb RAM, Intel core i7 11gen',price:900,
    image:'https://mobilestore.ec/wp-content/uploads/2022/02/MSI-GF63818-GAMING-LAPTOP-Mobile-Store-Ecuador1.jpg',
    quantity:10},
    {name:'Alienware',description:'Laptop Alienware, 500 gb SSD storage, 24gb RAM, NVIDIA RTX 3080, Intel core i5',price:1100,
    image:'https://http2.mlstatic.com/D_NQ_NP_690577-MLA41020340971_032020-O.webp',
    quantity:10},
    {name:'Apple',description:'Mac Book Pro M2, 512GB SSD storage, 16gb RAM, chip M2 de apple',price:1300,
    image:'https://ipadizate.com/hero/2022/06/MacBook-air-2022.jpg?width=1200',
    quantity:10}
]
items.each do |item_params|
    Item.find_or_create_by(item_params)
end 

#create customers 
customers = [
  { first_name: 'Paul', last_name: 'Garcia', email: 'psgarcia@puce.edu.ec'},
  { first_name: 'William', last_name: 'Guanin', email: 'william_7006@hotmail.com'}
  
]
customers.each do |customer_params|
  Customer.find_or_create_by(customer_params)
end

#create Customers_Addresses 
customers_addresses = [
  { customer_id: 1, street: 'Av.Mariscal Sucre', city: 'Quito'},
  { customer_id: 2, street: 'Av.Eloy Alfaro', city: 'Quito'}
  
]
customers_addresses.each do |customer_address_params|
  CustomerAddress.find_or_create_by(customer_address_params)
end



