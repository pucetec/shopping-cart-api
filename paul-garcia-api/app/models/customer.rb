class Customer < ApplicationRecord
    has_many :customer_addresses, dependent: :destroy 
    has_many :orders, dependent: :destroy
end
