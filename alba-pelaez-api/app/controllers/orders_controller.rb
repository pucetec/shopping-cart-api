class OrdersController < ApplicationController
    before_action :set_headers
    def index
      render json: Order.all
    end
    def create
        customer_email = params[:customer]
        customer = Customer.find_by(email: customer_email)
        if customer.nil?
          render json: {message: 'Customer not found'}
        else
          order = Order.new
          order.customer_id = customer.id
          order.save
          total = 0
          items = params[:items]
          items.each do |item|
            found_item = Item.find(item[:id])
            if found_item
              line_item = LineItem.new
              line_item.item_id = found_item.id
              line_item.order_id = order.id
              line_item.quantity = item[:quantity]
              line_item.unit_price = found_item.price
              line_item.save
              total += line_item.quantity * found_item.price
            end
          end
          order.total=total
          order.save
          
          render json: {customer: customer, order: order}
        end

      end
    
    
end
