# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)


i=Item.find_or_create_by(name: "Japanese Cherry Blossom")
i.description = "A qué huele: la fragancia equivalente a tu pequeño vestido negro: hermoso, atemporal e innegablemente femenino"
i.price = 25.50
i.image = 'https://bathbodyecu.vteximg.com.br/arquivos/ids/162631-650-709/026438528.jpg?v=637927310098370000'
i.save

i=Item.find_or_create_by(name:"A Thousand Wishes")
i.description = "Una celebración dulce y conmovedora"
i.price = 23
i.image ='https://bathbodyecu.vteximg.com.br/arquivos/ids/163450-650-709/026502533.jpg?v=638011094899630000'
i.save

i=Item.find_or_create_by(name: "Coco Paradise")
i.description = "A entrar en un oasis de lujo cálido, dulce y relajante"
i.price = 23.50
i.image = 'https://bathbodyecu.vteximg.com.br/arquivos/ids/165985-650-709/026594088.jpg?v=638197896747130000'
i.save

i=Item.find_or_create_by(name: "Watermelon Mojito")
i.description = "A un cóctel fresco y frutal"
i.price = 27
i.image ='https://bathbodyecu.vteximg.com.br/arquivos/ids/165990-650-709/026624888.jpg?v=638197896787730000'
i.save

i=Item.find_or_create_by(name: "Into the Night")
i.description = "Una noche atemporal, femenina y atractiva en la ciudad"
i.price = 25.5
i.image = 'https://bathbodyecu.vteximg.com.br/arquivos/ids/163448-650-709/026502467.jpg?v=638011094886130000'
i.save

i=Item.find_or_create_by(name: "Among the Clouds")
i.description = "A volar a través de las nubes — etéreo, fascinante y dulce."
i.price = 26
i.image = 'https://bathbodyecu.vteximg.com.br/arquivos/ids/165553-650-709/026589848.jpg?v=638167550582570000'
i.save


c=Customer.find_or_create_by(first_name: "Alba")
c.last_name= "Pelaez"
c.email= "avpelaez@puce.edu.ec"
c.save

c=Customer.find_or_create_by(first_name: "Mario")
c.last_name= "Contero"
c.email= "macontero@puce.edu.ec"
c.save

ca=CustomerAddress.find_or_create_by(streets: "12 de Octubre", city: "Quito")
ca.customer_id=1
ca.save


