class Customer < ApplicationRecord
    has_many :orders, dependent: :destroy
    has_many :customer_addresses, dependent: :destroy
end
