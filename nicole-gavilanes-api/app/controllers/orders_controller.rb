class OrdersController < ApplicationController
    before_action :set_headers

	def create
		successful_order = false

		@customer=Customer.find_by(email: params[:email])
		if @customer.nil?
			render json: {message:"User not found"}
		else
			order=Order.new
			order.customer_id=@customer.id
			order.description = "Order from client"
			order.save

			items = params[:items]
			puts items
			total = 0
			items_in_order=[]
			items.each do |item|
				found_item = Item.find(item["id"])
				if found_item
					line_item = LineItem.new
					line_item.item_id = found_item.id
					line_item.order_id = order.id
					line_item.quantity = item["quantity"]
					line_item.unit_price = found_item.price
					line_item.save
					item_data = { quantity: line_item.quantity, name: found_item.name } 
					items_in_order.push(item_data)
					total = total + (line_item.quantity * found_item.price)
				end
			end
			order.total = total
			order.save
			successful_order = true
		end
		if successful_order
			render :json => {:customer => @customer, :total => total, :items_in_order => items_in_order}
		end
	end
end