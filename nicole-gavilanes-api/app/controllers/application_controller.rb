class ApplicationController < ActionController::API
    def set_headers
        headers['Access-Control-Allow-Origin'] = "http://localhost:3001"
    end
end
