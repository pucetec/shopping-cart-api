# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

i=Item.find_or_create_by(name:"Christmas Sweater")
i.price= 30
i.description= "long sleeve ugly sweater"
i.image="https://cdn.shopify.com/s/files/1/1265/6377/products/200200084873_1.jpg?v=1669676413"
i.save

i=Item.find_or_create_by(name: "Christmas Headband")
i.price= 5
i.description= "reindeer antlers headband"
i.image="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEUrhFks1RlvJoOo9jfSjgfHhxYElBurV-YQ&usqp=CAU"
i.save

i=Item.find_or_create_by(name: "Christmas Hat")
i.price= 15
i.description= "classic Santa Claus hat"
i.image="https://i5.walmartimages.com/asr/55dd9cd9-b5e6-41ba-8636-dde1967e0aee.1e8c71cd26c04132b9fc5a40fe11265d.jpeg"
i.save

i=Item.find_or_create_by(name: "Christmas Earrings")
i.price= 10
i.description= "green glitter tree earrings"
i.image="https://i0.wp.com/itsjustsoyou.com/wp-content/uploads/2018/11/Glitter-Green-Christmas-Tree-Outline-Statement-Dangle-Earrings-6.jpg?resize=416%2C416&ssl=1"
i.save

i=Item.find_or_create_by(name: "Christmas Tie")
i.price= 10
i.description= "navy blue patterned tie"
i.image="https://shop.thetiebar.com/cdn/shop/products/31446.jpg?v=1613769144"
i.save

i=Item.find_or_create_by(name: "Christmas Handbag")
i.price= 70
i.description= "carols golden handbag"
i.image="https://www.maryfrances.com/cdn/shop/files/christmas-carols-top-handle-handbag-mary-frances-accessories-954.jpg?v=1689882521"
i.save

i=Item.find_or_create_by(name: "Christmas Pajamas")
i.price= 20
i.description= "set plaid pants sleepwear"
i.image="https://i5.walmartimages.com/asr/08e1d272-a93c-4bd9-85ec-8b41344068da.69d9266c98c0b8ce0b9306a41002f15b.jpeg"
i.save

i=Item.find_or_create_by(name: "Christmas Ornaments")
i.price= 8
i.description= "nutcracker tree decorations"
i.image="https://s.alicdn.com/@sc04/kf/Hbd57d008ba9549a1b53a04ebc549dcaec.jpg_960x960.jpg"
i.save

i=Item.find_or_create_by(name: "Christmas Socks")
i.price= 5
i.description= "candy cane crew socks"
i.image="https://www.jefferiessocks.com/media/catalog/product/cache/eae63facadd07cb2c2b77f73f8da6996/2/7/2779_candy-cane_z.jpg"
i.save

c=Customer.find_or_create_by(first_name: "Nicole")
c.last_name= "Gavilanes"
c.email= "nicole.gavilanes94@gmail.com"
c.save

c=Customer.find_or_create_by(first_name: "Camilo")
c.last_name= "Orejuela"
c.email= "camilorejuela@gmail.com"
c.save

c=Customer.find_or_create_by(first_name: "Sneyder")
c.last_name= "Tarco"
c.email= "sneydertarco@gmail.com"
c.save

c=Customer.find_or_create_by(first_name: "Sandra")
c.last_name= "Luzuriaga"
c.email= "sandraluzuriaga@gmail.com"
c.save

c=Customer.find_or_create_by(first_name: "Carlos")
c.last_name= "Moya"
c.email= "carlosmoya@gmail.com"
c.save

c=Customer.find_or_create_by(first_name: "Andres")
c.last_name= "Gavilanes"
c.email= "agavilanes@gmail.com"
c.save

ca=CustomerAddress.find_or_create_by(street: "12 de Octubre", city: "Quito")
ca.customer_id=1
ca.save

ca=CustomerAddress.find_or_create_by(street: "Av. 6 de diciembre", city: "Quito")
ca.customer_id=2
ca.save

ca=CustomerAddress.find_or_create_by(street: "Av. 10 de agosto", city: "Quito")
ca.customer_id=3
ca.save

ca=CustomerAddress.find_or_create_by(street: "Av. Quito", city: "Manta")
ca.customer_id=4
ca.save

ca=CustomerAddress.find_or_create_by(street: "Av. Brasil", city: "Quito")
ca.customer_id=5
ca.save

ca=CustomerAddress.find_or_create_by(street: "Av. Napo", city: "Quito")
ca.customer_id=6
ca.save

ca=CustomerAddress.find_or_create_by(street: "Av. Maldonado", city: "Quito")
ca.customer_id=7
ca.save