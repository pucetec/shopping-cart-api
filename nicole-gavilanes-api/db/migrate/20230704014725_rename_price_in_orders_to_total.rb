class RenamePriceInOrdersToTotal < ActiveRecord::Migration[7.0]
  def change
    rename_column :orders, :price, :total
  end
end
