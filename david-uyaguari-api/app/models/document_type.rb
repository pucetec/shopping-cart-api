class DocumentType < ApplicationRecord
    has_many :customers, dependent: :destroy
end
