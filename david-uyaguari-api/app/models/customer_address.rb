class CustomerAddress < ApplicationRecord
    belongs_to :customer, dependent: :destroy
end
