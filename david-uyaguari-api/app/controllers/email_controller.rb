class EmailController < ApplicationController
    def validate
        email = params[:email]
        email_exists = Customer.exists?(email: email)
        render json: { isValid: email_exists }
      end
end
