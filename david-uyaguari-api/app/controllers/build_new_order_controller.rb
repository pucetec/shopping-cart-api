class BuildNewOrderController < ApplicationController
    def create
        email = params[:email]
        items = params[:buyItems]
        customer = Customer.find_by(email: email)
        get_customer_id = customer.id
        new_order = Order.create(date: DateTime.now,customer_id: get_customer_id,payment_method_id:1)
        get_order_id = new_order.id
        totalPrice = 0
        items.each do |item|
            item_id = item[:id]
            cantidad = item[:quantity]
            current_item = Item.find_by(id: item_id)
            OrderDetail.create(order_id: get_order_id, item_id: current_item.id,description:"Status:OK",unit_price:current_item.price, quantity: cantidad,subtotal:(current_item.price)*cantidad,total:(current_item.price)*cantidad)
            totalPrice = totalPrice + (cantidad * current_item.price)
        end
        order_details = OrderDetail.includes(:item).where(order_id: get_order_id)
        customer = Customer.find_by(id: order_details.first&.order&.customer_id)
        document_type_name = customer&.document_type&.description
        order_details_data = order_details.map do |detail|
        item = detail.item
        {
            product_name: item&.name,  
            quantity: detail.quantity,
            unit_price: item&.price,
            subtotal: detail.subtotal
        }
        end
        order_info = {
        customer_name: customer&.first_name + ' ' + customer&.last_name,
        document_number: customer&.document_number,
        document_type_name: document_type_name,
        order_number: new_order.id,
        total: totalPrice
        }
        data = {
        order_info: order_info,
        order_details: order_details_data
        }
        render json: data
    end
end
