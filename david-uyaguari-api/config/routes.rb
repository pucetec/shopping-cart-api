Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get '/products', to: 'products#index'
  post '/validate_email', to: 'email#validate'
  post '/build_order', to: 'build_new_order#create'
end
