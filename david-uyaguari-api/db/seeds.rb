# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
items = [
    { name:' Xbox: Starfield Limited Edition', description:'Descubre cómo la cruceta metálica de bronce complementa el simple diseño técnico de la tapa superior de color blanco. La marca de color inspirada en Constellation alrededor del botón Xbox sirve como un recordatorio visual del viaje épico que te espera: “Para todos, hacia Starfield”*', price:81.61, quantity:20,image:"https://assets.xboxservices.com/assets/90/68/90685444-2ea5-4936-bdbe-510a6c601a41.jpg?n=1029384_Gallery-0_3_1350x759.jpg"},
    { name:'Xbox: Stellar Shift Special Edition', description:'Desata el surrealismo con el Mando inalámbrico Xbox: Stellar Shift Special Edition, con cruceta híbrida que incluye agarres con remolinos y centelleo azules-violeta que cambian de color, así como agarres texturizados con remolinos de color púrpura. Usa la app Accesorios de Xbox para reasignar botones y ,crear perfiles de mando personalizados. Empareja rápidamente, juega y cambia rápidamente entre dispositivos, como consola, PC y dispositivo móvil*', price:76.17, quantity:20, image:"https://assets.xboxservices.com/assets/34/3d/343d33f5-9e8b-4cfd-a9d1-3a8586a149a1.jpg?n=878621_Gallery-0_1_1350x759.jpg"},
    { name:'XBOX SERIES S', description:'Prepárate para tiempos de carga más rápidos, mayor resolución, velocidades de fotogramas más estables y mejor latencia de entrada en miles de juegos para Xbox One, Xbox 360 y Xbox Original.', price:326.47, quantity:20, image: "https://assets.xboxservices.com/assets/0b/28/0b2854b9-a7e7-47dd-b4f8-a371567854b2.png?n=Xbox-Series-S_Buy-Box_0_01_829x799.png"},
    { name:'XBOX SERIES X', description:'La arquitectura de refrigeración paralela de Xbox está hecha con tres canales de flujo de aire que distribuyen uniformemente las temperaturas que generan los avanzados componentes internos, por lo que la consola no se calienta ni hace ruido.', price:544.13, quantity:20, image: "https://assets.xboxservices.com/assets/fb/d2/fbd2cb56-5c25-414d-9f46-e6a164cdf5be.png?n=XBX_A-BuyBoxBGImage01-D.png"}
]
items.each do |item_params|
  Item.find_or_create_by(item_params)
end

dtypes = [
    { description:'cedula'},
    { description:'RUC'},
    { description:'Pasaporte'}
    ]
dtypes.each do |dtypes_params|
  DocumentType.find_or_create_by(dtypes_params)
end

pmethods= [
    { description:'Transferencia'},
    { description:'Tarjeta Credito'},
    { description:'Tarjeta Debito'}
    ]
pmethods.each do |pmethods_params|
    PaymentMethod.find_or_create_by(pmethods_params)
end
customers= [
    { document_type_id:2,document_number:'1726685743001', first_name:'David',last_name:'Uyaguari', email:'dauyaguari@puce.edu.ec',phone:'0999750604'},
    { document_type_id:1,document_number:'0905574264', first_name:'Prueba',last_name:'Parcial', email:'prueba@prueba.com',phone:'0999999999'},
    { document_type_id:3,document_number:'A0755B5', first_name:'Ellen',last_name:'Ripley', email:'eripley@puce.edu.ec',phone:'0999860347'}
    ]
customers.each do |customers_params|
    Customer.find_or_create_by(customers_params)
end

caddresses= [
    { main_street:'Av 12 de Octubre', secondary_street:'Vicente Ramón', house_number:'1076', city:'Quito', state:'Pichincha', country:'Ecuador',customer_id:1},
    { main_street:'Av Naciones Unidas', secondary_street:'Av 6 de Diciembre', house_number:'2040', city:'Quito', state:'Pichincha', country:'Ecuador',customer_id:4},
    { main_street:'Av 6 de Diciembre', secondary_street:'Whimper', house_number:'887', city:'Quito', state:'Pichincha', country:'Ecuador',customer_id:3}
    ]
    caddresses.each do |caddresses_params|
    CustomerAddress.find_or_create_by(caddresses_params)
end

