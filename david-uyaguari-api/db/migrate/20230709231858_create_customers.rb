class CreateCustomers < ActiveRecord::Migration[7.0]
  def change
    create_table :customers do |t|
      t.bigint :document_type_id, null: false
      t.string :document_number, null: false, unique: true
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :email, null: false
      t.string :phone, null: false
      t.boolean :is_deleted, default:false
      t.timestamps
    end
  end
end
