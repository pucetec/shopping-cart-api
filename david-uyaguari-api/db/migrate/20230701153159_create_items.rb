class CreateItems < ActiveRecord::Migration[7.0]
  def change
    create_table :items do |t|
      t.string:name
      t.text:description
      t.decimal:price, precision:10, scale: 2
      t.integer:quantity
      t.string:image
      t.boolean:is_deleted, default: false
      t.timestamps
    end
  end
end
