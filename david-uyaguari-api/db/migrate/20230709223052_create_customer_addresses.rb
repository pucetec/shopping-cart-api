class CreateCustomerAddresses < ActiveRecord::Migration[7.0]
  def change
    create_table :customer_addresses do |t|
      t.string:main_street, null: false
      t.string:secondary_street, null: false
      t.string:house_number, null: false
      t.string:city,null: false
      t.string:state,null: false
      t.string:country,null: false
      t.bigint:customer_id, null: false
      t.boolean:is_deleted, default: false
      t.timestamps
    end
  end
end
