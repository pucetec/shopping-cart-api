class CreateDocumentTypes < ActiveRecord::Migration[7.0]
  def change
    create_table :document_types do |t|
      t.string:description ,null: false
      t.boolean:is_deleted, default: false
      t.timestamps
    end
  end
end
