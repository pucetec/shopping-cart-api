class ChangeUnitPriceToDecimalInOrderDetails < ActiveRecord::Migration[7.0]
  def change
    change_column :order_details, :unit_price, :decimal, precision: 10, scale: 2, null: false
  end
end
