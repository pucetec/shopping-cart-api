# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Coffee.find_or_create_by!(
  name: 'Black',
  description: 'A strong coffe',
  price: 3.5,
  available_quantity: 20
)

Coffee.find_or_create_by!(
  name: 'Latte',
  description: 'Served as expresso or steamed milk',
  price: 4.5,
  available_quantity: 19
)

Coffee.find_or_create_by!(
  name: 'Cappuccino',
  description: 'Served as expresso, steamed milk and foam',
  price: 5,
  available_quantity: 7
)