class CoffeesController < ApplicationController
  before_action :set_headers

  def index
    render json: Coffee.all
  end
end
