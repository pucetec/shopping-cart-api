class OrdersController < ApplicationController
  def index
    render json: Order.all
  end

  def create
    customer_email = params[:customerEmail][:params][:email]
    cart_items_details = params[:cartItemsDetails]

    customer = Customer.find_by(email: customer_email)

    if customer
      get_customer_id = customer.id
      order = Order.new(date_time: Time.now, customer_id: get_customer_id, status: 'Aceptado')

      if order.save
        total_amount = 0

        cart_items_details.each do |item|
          product = Product.find(item[:id])
          quantity = item[:quantity]
          unit_price = product.price
          total_amount += unit_price * quantity

          OrderDetail.create(
            order: order,
            product: product,
            quantity: quantity,
            unit_price: unit_price
          )
        end

        order.update(description: "Orden de compra #: #{order.id}", total: total_amount)

        order_info = {
          order_id: order.id,
          customer: {
            identification_type: customer.identification_type.description,
            identification_number: customer.identification_number,
            full_name: "#{customer.firstname} #{customer.lastname}",
            phone: customer.phone,
            address: "#{customer.customer_addresses.first.first_street},
            #{customer.customer_addresses.first.street_number},
            #{customer.customer_addresses.first.second_street}",

            email: customer.email,
            city: customer.customer_addresses.first.city
          },
          order_details: order.order_details.map do |detail|
            {
              product_name: detail.product.name,
              quantity: detail.quantity,
              unit_price: detail.unit_price
            }
          end,
          total_amount: order.total
        }

        render json: order_info
      else
        render json: { error: 's' }, status: :unprocessable_entity
      end
    else
      render json: { error: 'El cliente no existe en la base de datos' }, status: :unprocessable_entity
    end
  end
end
