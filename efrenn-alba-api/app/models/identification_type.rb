class IdentificationType < ApplicationRecord
    has_many :customers, dependent: :destroy
end
