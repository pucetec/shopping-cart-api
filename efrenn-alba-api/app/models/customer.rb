class Customer < ApplicationRecord
    has_many :customer_addresses, dependent: :destroy
    belongs_to :identification_type
    has_many :orders, dependent: :destroy
end
