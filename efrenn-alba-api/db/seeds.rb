# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

# Create_Identification_Types
identifications = [
  { description: 'Cédula', is_deleted: false },
  { description: 'R.U.C.', is_deleted: false },
  { description: 'Pasaporte', is_deleted: false }
]

identifications.each do |identification_params|
  IdentificationType.find_or_create_by(identification_params)
end

# Create_Customers
customers = [
  { identification_type_id: 1, identification_number: '1726616483', firstname: 'Efrenn',
    lastname: 'Alba', email: 'epalba@puce.edu.ec',
    phone: '0978632798', is_deleted: false },
  { identification_type_id: 2, identification_number: '1712267770001', firstname: 'Carlos',
    lastname: 'Manosalvas', email: 'cmanosalvasm@hotmail.com',
    phone: '0982061027', is_deleted: false }
]

customers.each do |customer_params|
  Customer.find_or_create_by(customer_params)
end

# Create_Customers_Addresses
addresses = [
  { customer_id: 1, first_street: 'Jose Plaza', second_street: 'Gonzalo Pizarro', street_number: 'E2-33',
    city: 'Quito', state: 'Pichincha', is_deleted: false },
  { customer_id: 2, first_street: 'Interoceánica', second_street: 'Manuel Soria', street_number: 'N8-101',
    city: 'Quito', state: 'Pichincha', is_deleted: false }
]

addresses.each do |address_params|
  CustomerAddress.find_or_create_by(address_params)
end

# Create_Products
products = [
  { name: 'Dell Latitude Notebook 3250',
    description: 'Intel Core i9 13th Gen., 32GB RAM, 1TB SSD, Windows 11 Professional',
    available_stock: 5, price: 2499.99, image: './images/items/notebook_3250.webp', is_deleted: false },
  { name: 'Dell Latitude All-In-One 2435',
    description: 'Intel Core i7 12th Gen., 32GB RAM, 450GB SSD, Windows 11 Professional',
    available_stock: 5, price: 1245.50, image: './images/items/aio_computer_2435.webp', is_deleted: false },
  { name: 'Dell Monitor 1945G', description: '1 Display Port, 2 HDMI, Camera Web Integrated, 3 USB 3.0 HUB Ports',
    available_stock: 5, price: 400.00, image: './images/items/monitor_1945g.webp', is_deleted: false },
  { name: 'Dell Web Camera 1080FX', description: '1080 Video Recording, Night Mode', available_stock: 5, price: 150,
    image: './images/items/wc_1080fx.webp', is_deleted: false },
  { name: 'Dell Dock-Station 2H1WC', description: '2 USB 3.0, Smartphone Stand, 10w Wireless Charge',
    available_stock: 5, price: 150.80, image: './images/items/ds_2h1wc.webp', is_deleted: false }
]

products.each do |product_params|
  Product.find_or_create_by(product_params)
end
