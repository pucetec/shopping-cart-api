class AddProductRefToOrderDetails < ActiveRecord::Migration[7.0]
  def change
    add_foreign_key :order_details, :products
  end
end
