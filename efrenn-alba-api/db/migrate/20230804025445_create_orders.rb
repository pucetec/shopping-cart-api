class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.datetime :date_time, null: false
      t.bigint :customer_id, null: false
      t.string :description, null: false, default: "Procesando Orden"
      t.decimal :total, precision: 10, scale: 2, default: 0, null: false
      t.string :status, null: true
      t.boolean :is_deleted, null: false, default: false
      t.timestamps
    end
  end
end
