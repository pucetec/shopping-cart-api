class CreateOrderDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :order_details do |t|
      t.bigint :order_id, null: false
      t.bigint :product_id, null: false
      t.decimal :unit_price, precision: 10, scale: 2, default: 0, null: false
      t.integer :quantity, null: false
      t.boolean :is_deleted, null: false, default: false
      t.timestamps
    end
  end
end
