class AddIdentificationTypeRefToCustomers < ActiveRecord::Migration[7.0]
  def change
    add_foreign_key :customers, :identification_types
  end
end
