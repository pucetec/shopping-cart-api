class CreateCustomerAddresses < ActiveRecord::Migration[7.0]
  def change
    create_table :customer_addresses do |t|
      t.bigint :customer_id, null: false
      t.string :first_street, null: false
      t.string :second_street, null: false
      t.string :street_number, null: false
      t.string :city, null: false
      t.string :state, null: false
      t.boolean :is_deleted, null: false, default: false
      t.timestamps
    end
  end
end
