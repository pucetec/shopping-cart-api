class AddOrderRefToOrderDetails < ActiveRecord::Migration[7.0]
  def change
    add_foreign_key :order_details, :orders
  end
end
