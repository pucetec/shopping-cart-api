class OrdersController < ApplicationController
    

    def create
        total = 0
        order_created = false
                
        @customer = Customer.find_by_email(params[:payload][:customer])
        if @customer.nil?
          render json: { message: "Usuario no encontrado." }
        else
          order= Order.new
          order.customer_id = @customer.id
          order.description = "Nuestra primera orden"
          order.save

        
        
        item = params[:payload][:selectedItems]    

        item.each do |item|
          found_item = Item.find(item["id"])
          if found_item
            line_item = LineItem.new
            line_item.item_id = found_item.id
            line_item.order_id = order.id
            line_item.quantity = item["quantity"]
            line_item.unit_price = found_item.price
            line_item.save
            total =  total + item["quantity"]* found_item.price

          end
        end

        order.total = total
        order.save
        
        order_created = true

        if order_created
          render json: {order: order, customer: @customer, items: order.items}
        else
          render json:  {message: "Accion rechazada, intentelo nuevamente"}
        end
      end
        

   
     end
end
