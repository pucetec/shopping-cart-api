class AddImageToItem < ActiveRecord::Migration[7.0]
  def change
    rename_column :items, :descrpcion, :description
    add_column :items, :image, :text
  end
end
