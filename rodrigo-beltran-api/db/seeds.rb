# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)




i = Item.find_or_create_by(name:"Electrolux - Lavadora automática EWIX21F6ESB Negro | 21Kg")
i.image = "https://marcimex.vtexassets.com/arquivos/ids/196640-300-300?v=1773012318&width=300&height=300&aspect=true"
i.price = 419.0
i.description = "Especificaciones: Tipo de Carga: Superior, Capacidad Kg : 21Kg, Material del Tambor: Acero Inoxidable, Garantia: (1 Año Producto, 10 Años Motor), Color: Negro"
i.save

i = Item.find_or_create_by(name:"Tcl - Aire Acondicionado TAC12CSA | 12000 BTU")
i.image = "https://marcimex.vtexassets.com/arquivos/ids/188518-300-300?v=1773011613&width=300&height=300&aspect=true"
i.price = 297.17
i.description = "-Especificaciones:Color BlancoCapacidad 12.000 BTUVoltaje / Frecuencia 220V/60HzGas refrigerante R410Reinicio automático"
i.save

i = Item.find_or_create_by(name:"Global - Refrigeradora RG-8 Steel | 176 Litros")
i.image = "https://marcimex.vtexassets.com/arquivos/ids/203165-1200-auto?v=638216767071400000&width=1200&height=auto&aspect=true"
i.price = 199.0,
i.description = "Información general: Eficiencia Energética A, Disponible en Steel, Refrigeración R600, Dimensiones: Alto: 128 cm, Ancho: 52 cm, Profundidad: 54 cm"
i.save

i = Item.find_or_create_by(name:"LG - Secadora a Gas DT21VS 21 Kg | Silver")
i.image = "https://marcimex.vtexassets.com/arquivos/ids/184270-1200-auto?v=637813924244700000&width=1200&height=auto&aspect=true"
i.price = 638.97
i.description = "Motor 10 años de Garantía, Conveniencia: Smart Diagnosis  Diseño:Simple front control panel with LED display  Capacidad: 21 Kg  Dimensiones (An x prof x lar) = 686 x 750 x 1020."
i.save

i = Item.find_or_create_by(name:"Indurama - Cocina a Gas Montecarlo Zafiro | Croma")
i.image = "https://marcimex.vtexassets.com/arquivos/ids/184517-1200-auto?v=637822709535170000&width=1200&height=auto&aspect=true"
i.price = 778.96
i.description = " Alto: 95 cm, Ancho: 76 cm, Prof: 64 cm, Acabado en acero inoxidable, Tapa de vidrio templado, Tablero de acero inoxidable, Bisagras incorporadas, Parrillas de hierro fundido"
i.save

i = Item.find_or_create_by(name:"Global - Microondas MOG20LS0001 | 20 Litros")
i.image = "https://marcimex.vtexassets.com/arquivos/ids/199017-1200-auto?v=638145861744500000&width=1200&height=auto&aspect=true"
i.price = 95.0
i.description = "Características: Potencia 700 W, 10 niveles de potencia, Display led con reloj, Sistema eficiente de cocción, Temporizador, Botón de apertura, Plato giratorio de cristal, Menú de cocción rápida, Descongelamiento rápido, Bajo consumo energético"
i.save

i = Item.find_or_create_by(name:"Challenger - Campana extractora CX4865 | Inox")
i.image = "https://marcimex.vtexassets.com/arquivos/ids/186815-1200-auto?v=637860585780970000&width=1200&height=auto&aspect=true"
i.price = 201.0
i.description = " 3 velocidades que garantiza una extracción eficaz de vapores y olores, Control frontales, Iluminación LED, Filtro de carbón activado, Filtro atrapa grasa en aluminio y lavable, Color acero inoxidable, Dimensiones: alto 13 x ancho 60 x profundidad 49,5 cm, Distancia del mesón al mueble de instalacion 75cm"
i.save

i = Item.find_or_create_by(name:"I Glace - Refrigeradora Ig-R200 | Inox")
i.image = "https://marcimex.vtexassets.com/arquivos/ids/201334-1200-auto?v=638188282106600000&width=1200&height=auto&aspect=true"
i.price = 849.0
i.description = "Especificaciones: Incluye termostato eléctrico, Estante deslizable - Luz LED interior azul, Temperatura interior de 0-10ºC, Frecuencia y voltaje: 110V/60Hz, Espacio interno disponible: 145 L, Peso Neto: 43 kg"
i.save

i = Item.find_or_create_by(name:"iGlace - Fridge Portátil 35Lts iG-CarV2 | Negro")
i.image = "https://marcimex.vtexassets.com/arquivos/ids/201333-1200-auto?v=638188280726670000&width=1200&height=auto&aspect=true"
i.price = 349.0
i.description = "Especificaciones: Capacidad de almacenamiento: 35 L, Panel digital para control de temperatura, Resistente a lluvia y arena, Ideal para viajar, Soportes de plástico, Con adaptador para cargar en el auto, Corriente: 110V-240V, Corriente Adaptador: Auto 12V y 24V, Refrigerante: R134A 2.2 Oz/62g, Tipo de enfriamiento: aire, Potencia de consumo: 66W/72W, Peso Neto: 31,.9 lb"
i.save

i = Item.find_or_create_by(name: "Home Elements - Combo Estufa Inducción + Sartén 20 cm")
i.image = "https://marcimex.vtexassets.com/arquivos/ids/202947-300-300?v=1772986026&width=300&height=300&aspect=true"
i.price = 69.0
i.description = "Especificaciones: Exclusivo con elementos inducción., No genera calor al tacto, se activa al entrar en contacto con elementos con inducción., Ahorra energía y tiempo., Botón de encendido y apagado., Base antideslizante."
i.save


c1 = Customer.find_or_create_by(first_name: "Rodrigo", last_name: "Beltran")
c1.email = "rhbeltran@puce.edu.ec"
c1.save
c2 = Customer.find_or_create_by(first_name: "Gabriela", last_name: "Monge")
c2.email = "gmonge@outlook.es"
c2.save
c3 = Customer.find_or_create_by(first_name: "Gabriel", last_name: "Beltran")
c3.email = "galexandrob@hotmail.com"
c3.save
c4 = Customer.find_or_create_by(first_name: "Sheyla", last_name: "Samaniego")
c4.email = "sheylasamaniego@hotmail.com"
c4.save


ca = CustomerAddress.find_or_create_by(customer_id: c1.id)
ca.street = "Av. Jorge Perez Concha y Ciruelos"
ca.city = "Quito"
ca.save

ca = CustomerAddress.find_or_create_by(customer_id: c2.id)
ca.street= "Av. Amazonas y 10 de agosto"
ca.city = "Salgonqui"
ca.save

ca = CustomerAddress.find_or_create_by(customer_id: c3.id)
ca.street= "Av. Av. Los Cerezos y los Tulipanes"
ca.city = "Ibarra"
ca.save

ca = CustomerAddress.find_or_create_by(customer_id: c4.id)
ca.street= "Av. 6 de Diciembre y Rio Coca"
ca.city = "Quito"
ca.save