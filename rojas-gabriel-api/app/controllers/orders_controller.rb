class OrdersController < ApplicationController
    def indexorder
        email = params[:email]
        items = params[:buyItems]
        customer = Customer.find_by(email: email)
        get_customer_id = customer.id
        new_order = Order.create(name: customer.name,customer_id: get_customer_id)
        get_order_id=new_order.id
        totalPrice = 0
        items.each do |item|
            item_id = item[:id]
            cantidad = item[:cantidad]
            current_item = Item.find_by(id: item_id)
            LineItem.create(order_id: get_order_id, item_id: current_item.id,unit_price:current_item.price, quantity: cantidad)
            totalPrice = totalPrice + (cantidad * current_item.price)
        end
        render json: {customer: customer,order: new_order, totalPrice: totalPrice}
    end
end