class Customer < ApplicationRecord
    has_many :customers_adresses,dependent: :destroy
    has_many :orders, dependent: :destroy
    belongs_to :document_type
end
