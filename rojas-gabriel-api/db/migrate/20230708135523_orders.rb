class Orders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.references :customer, foreign_key: true
      t.text :name
      t.decimal :price, precision:10, scale: 2
    end
  end
end
