class CreateCustomerAddresses < ActiveRecord::Migration[7.0]
  def change
    create_table :customer_addresses do |t|
      t.string :street
      t.string :city
      t.string :zipcode
      t.timestamps
    end
  end
end
