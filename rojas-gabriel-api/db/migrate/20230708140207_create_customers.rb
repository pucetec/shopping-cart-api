class CreateCustomers < ActiveRecord::Migration[7.0]
  def change
    create_table :customers do |t|
      t.string :name
      t.text :address
      t.text :city
      t.text :country
      t.text :email 
      t.timestamps
    end
  end
end
