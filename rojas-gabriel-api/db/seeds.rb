# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

items=[
    { name:'Samsung S10', description:'Celular nuevo y unico', price:500.30, image: 'https://crecos.vtexassets.com/arquivos/ids/170145-800-auto?v=636934496185500000&width=800&height=auto&aspect=true'},
    { name:'Samsung S20', description:'Celular nuevo y unico', price:1500.30, image: 'https://crecos.vtexassets.com/arquivos/ids/170145-800-auto?v=636934496185500000&width=800&height=auto&aspect=true'},
    { name:'Samsung S22', description:'Celular nuevo y unico', price:1560.30, image: 'https://crecos.vtexassets.com/arquivos/ids/170145-800-auto?v=636934496185500000&width=800&height=auto&aspect=true'}
]
items.each do |item_params|
    Item.find_or_create_by(item_params)
  end


customers=[
    {name: 'Gabriel Rojas', address: 'Pedro de Alvarado', city: 'Quito', country: 'Ecuador', email: 'leogaborojas1@gmail.com'},
    {name: 'Xavier Veracruz', address: 'Sabanilla', city: 'Quito', country: 'Ecuador', email: 'zubzer25@gmail.com'}
]
customers.each do |customers_params|
    Customer.find_or_create_by(customers_params)
end

