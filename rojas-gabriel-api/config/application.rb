require_relative "boot"

require "rails/all"
require 'rack/cors'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RojasGabrielApi
  class Application < Rails::Application
  config.middleware.insert_before 0, Rack::Cors do
    allow do
      origins 'http://localhost:3000' 
      resource '*', headers: :any, methods: [:get, :post, :put, :patch, :delete, :options, :head]
    end
  end
end
end
