# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
i=Item.find_or_create_by(name:"Apple Watch")
i.price= 879
i.description= "The wide, always active screen of the Apple Watch Series 8 extends to the ultra-thin. Your ideal partner is even more powerful"
i.image="https://i5.walmartimages.com/asr/6baddfd8-2651-4832-817d-fbe4c5bd04df.ad0084a22a4e5a3f05b7326e582b975c.jpeg"
i.save

i=Item.find_or_create_by(name:"Ipad")
i.price= 650
i.description= "The new iPad falls in love at first sight. It has a 10.9 inch Liquid Retina display with edge to edge design perfect for working"
i.image="https://www.apple.com/v/ipad-10.9/b/images/overview/hero/hero__ecv967jz1y82_large.jpg"
i.save

i=Item.find_or_create_by(name:"Mac Pro")
i.price= 5699
i.description= "Exceptional performance that already comes from the factory. The new Mac Pro combines Apples chip performance and PCIe expansion"
i.image="https://www.mresell.es/wp-content/uploads/2019/12/mac-pro-late-2019.jpg"
i.save

i=Item.find_or_create_by(name:"MacBook Pro")
i.price= 1350
i.description= "Thanks to the new M2 chip, the 13-inch MacBook Pro is more powerful than ever. Offers up to 20 hours of battery and a active cooling system"
i.image="https://www.apple.com/newsroom/images/product/mac/standard/Apple_MacBook-Pro_14-16-inch_10182021_big.jpg.large.jpg"
i.save

i=Item.find_or_create_by(name:"AirPods")
i.price= 123
i.description= "AirPods give you an incredible wireless experience, as they now offer you many hours of audio and conversation and the possibility of using them"
i.image="https://mundomac.com.ec/wp-content/uploads/2021/11/MV7N2.jpeg"
i.save

i=Item.find_or_create_by(name:"MacBook Air")
i.price= 1125
i.description= "Incredibly thin and fast, to work, play and create anywhere. 13-inch model with M2 chip now available Check the availability later of the 15-inch model"
i.image="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMEZAu4vNLOj4gw5DmbsiLriOb3Q06IAUJGA&usqp=CAU"
i.save

i=Item.find_or_create_by(name:"Mac Studio")
i.price= 769
i.description= "The Mac Studio is undoubtedly the great darling of creative professionals around the world. Thanks to the super fast M2 Max chip and the revolutionary M2 Ultra"
i.image="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTWVnaBhCnexTcw59FX-M2-P4KIsIM6skMMOw&usqp=CAU"
i.save

i=Item.find_or_create_by(name:"Pro Display XDR")
i.price= 7650
i.description= "The worlds first 32-inch Retina 6K display. Up to 1,600 nits of brightness. An incredible 1,000,000:1 contrast ratio and super-wide viewing angle. More than a billion colors"
i.image="https://i.blogs.es/791fa3/apple_mac-pro-display-pro_display-pro_060319/450_1000.jpg"
i.save

#create customers 
customers = [
  { first_name: 'Eylul', last_name: 'Kaya', email: 'eyu.09@gmail.com'},
  { first_name: 'Burak', last_name: 'Demir.', email: 'burk97@hotmail.com'},
  { first_name: 'Nicole', last_name: 'Gavilanes', email: 'nsgavilanes@puce.edu.ec'},
  { first_name: 'Eduardo', last_name: 'Moran', email: 'edu.678@gmail.com'} 
]

customers.each do |customer_params|
    Customer.find_or_create_by(customer_params)
  end

  customer_addresses = [
    { customer_id: 1, street: '123 Main St', city: 'Example City' },
    { customer_id: 2, street: '456 Elm St', city: 'Another City' },
    { customer_id: 3, street: '789 Oak St', city: 'Yet Another City' },
    { customer_id: 4, street: '321 Pine St', city: 'Some City' }
  ]
  
  customer_addresses.each do |customer_address_params|
    CustomerAddress.find_or_create_by(customer_address_params)
  end