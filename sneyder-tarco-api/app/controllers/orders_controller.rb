class OrdersController < ApplicationController
    before_action :set_headers
  
    def create
      customer = Customer.find_by(email: params[:email])
  
      if customer.nil?
        render json: { message: "User not found" }
      else
        render json: { message: "Successful Purchase", customer: customer }
      end
    end
  end