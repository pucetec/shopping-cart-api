class CustomersController < ApplicationController
  def validate
    email = params[:email]
    customer = Customer.find_by(email: email)

    if customer
      render json: { isValid: true, customer_id: customer.id }
    else
      render json: { isValid: false }
    end
  end
end
