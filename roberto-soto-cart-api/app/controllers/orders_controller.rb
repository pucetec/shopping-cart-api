class OrdersController < ApplicationController
    def create
      customer_id = params[:customer_id]
      items = params[:items]
  
      order = Order.create(customer_id: customer_id, description: 'Order description', total: calculate_total(items))
  
      items.each do |item_params|
        LineItem.create(item_id: item_params[:item_id], order_id: order.id, quantity: item_params[:quantity], unit_price: item_params[:unit_price])
      end
  
      render json: { success: true, order: order }
    end
  
  
    def calculate_total(items)
      items.sum { |item| item[:quantity].to_i * item[:unit_price].to_f }
    end

    def confirm 
        customer_id = params[:customer_id]
        items = params[:items]
        email = params[:email]

        customer = Customer.find(customer_id)
        order = Order.create(customer_id: customer_id, description: 'Order description', total: calculate_total(items))
        total_order=0
        items.each do |item_params|
            LineItem.create(item_id: item_params[:item_id], order_id: order.id, quantity: item_params[:quantity], unit_price: item_params[:unit_price])
            total_order += (item_params[:quantity].to_i * item_params[:unit_price].to_f)
        end

        render json: { success: true, customer:customer, order: order, final_total: order.total, email: email }

    end
    
  end
  