# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
items = [
  { name: 'Adidas Yeezy V05', price: 150.5, description: 'Adidas limited edition', available_quantity: 5 },
  { name: 'Jordan 1', price: 220.49, description: 'Jordan Retro 1', available_quantity: 15 },
  { name: 'Nike AF1', price: 150.5, description: 'Nike Air Force One', available_quantity: 20 }
]

items.each do |item_params|
  Item.find_or_create_by(item_params)
end

customers = [
  { first_name: 'Benito', last_name: 'Martinez', email: 'benito@gmail.com' },
  { first_name: 'Austin', last_name: 'Santos', email: 'astin@gmail.com' },
  { first_name: 'Raymond', last_name: 'Ayala', email: 'dy@gmail.com' }
]

customers.each do |customer_params|
  Customer.find_or_create_by(customer_params)
end

customers_addresses = [
  { customer_id: 1, street: "Calle Principal 1", city: "Ciudad 1" },
  { customer_id: 2, street: "Calle Principal 2", city: "Ciudad 2" },
  { customer_id: 3, street: "Calle Principal 3", city: "Ciudad 3" }
]

customers_addresses.each do |customer_address_params|
  CustomerAddress.find_or_create_by(customer_address_params)
end

create_line_items = [
  { item_id: 1, order_id: 1, quantity: 2, unit_price: 10 },
  { item_id: 2, order_id: 1, quantity: 1, unit_price: 15 },
  { item_id: 3, order_id: 2, quantity: 3, unit_price: 20 }
]

create_line_items.each do |line_item_params|
  LineItem.find_or_create_by(line_item_params)
end


create_orders = [
  { customer_id: 1, description: "Order 1", total: 100 },
  { customer_id: 2, description: "Order 2", total: 150 },
  { customer_id: 1, description: "Order 3", total: 200 }
]

create_orders.each do |order_params|
  Order.find_or_create_by(order_params)
end


