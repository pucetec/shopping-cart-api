require_relative "boot"
require 'rack/cors'
require "rails/all"

Bundler.require(*Rails.groups)

module RobertoSotoCartApi
  class Application < Rails::Application
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins 'http://localhost:3000' 
        resource '*', headers: :any, methods: [:get, :post, :put, :patch, :delete, :options, :head]
      end
    end

  end
end

