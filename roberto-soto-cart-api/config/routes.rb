Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  resources :items
  post '/validate_email', to: 'customers#validate'
  post '/create_order', to: 'orders#create'
  post '/confirm_order', to: 'orders#confirm'


  # Defines the root path route ("/")
  # root "articles#index"
end
