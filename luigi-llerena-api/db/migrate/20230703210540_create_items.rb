class CreateItems < ActiveRecord::Migration[7.0]
  def change
    create_table :items do |t|
      t.timestamps
      t.string :name
      t.text :description
      t.decimal :price, precision: 10, scale: 2
      t.text :imagen
    end
  end
end
