class ChangeAndRenameInTableItems < ActiveRecord::Migration[7.0]
  def change
    change_table :items do |t|
      t.string :procesador
      t.string :sistema_operativo
      t.string :memoria
      t.string :disco_duro
      t.string :chasis
    end

    remove_column :items, :description
    rename_column :items, :imagen, :image
  end
end
