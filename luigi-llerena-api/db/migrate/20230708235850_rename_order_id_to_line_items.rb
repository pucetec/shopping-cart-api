class RenameOrderIdToLineItems < ActiveRecord::Migration[7.0]
  def change
    rename_column :line_items, :orders_id, :order_id
  end
end
