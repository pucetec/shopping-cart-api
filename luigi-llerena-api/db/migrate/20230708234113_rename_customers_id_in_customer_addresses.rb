class RenameCustomersIdInCustomerAddresses < ActiveRecord::Migration[7.0]
  def change
    rename_column :customer_addresses, :customers_id, :customer_id
  end
end
