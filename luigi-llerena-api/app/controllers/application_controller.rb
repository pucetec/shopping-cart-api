class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  def set_cors_headers
  end
end
