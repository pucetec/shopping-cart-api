class CustomersController < ApplicationController
  def create
    email = params[:email]
    customer_data = Customer.find_by_email(email)

    if customer_data
      render json: { exists: true, customer: customer_data }
    else
      render json: { exists: false, customer: nil }
    end
  end
end