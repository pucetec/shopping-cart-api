class OrdersController < ApplicationController
  def create
    line_items = params[:order][:line_items]
    customer_id = params[:order][:customer_id]
    puts "ID #{customer_id} , #{line_items}"

    if line_items.nil? && customer_id.nil?
      render json: { error: "Line items and customer ID are required." }, status: :unprocessable_entity
      return
    end

    @order = handle_order(customer_id)
    return if @order.blank?

    handle_line_items(@order, line_items)
    calculate_total(@order)

    @order.description = 'Compra exitosa 2'

    if @order.save
      @order.line_items.each(&:save)
      render json: { order: @order.as_json(include: :items) }, status: :created
    else
      render json: { error: @order.errors.full_messages.join(', ') }, status: :unprocessable_entity
    end
  end

  private

  def handle_order(customer_id)
    begin
      Order.new(customer_id: customer_id)
    rescue ActiveRecord::RecordNotFound
      render json: { error: "Customer not found with ID: #{customer_id}" }, status: :not_found
      nil
    end
  end

  def handle_line_items(order, line_items)
    line_items.each do |item_params|
      order.line_items.build(item_id: item_params[:id], quantity: item_params[:quantity], unit_price: item_params[:unit_price])
    end
  end

  def calculate_total(order)
    order.total = order.line_items.sum { |item| item.quantity * item.unit_price }
  end
end
