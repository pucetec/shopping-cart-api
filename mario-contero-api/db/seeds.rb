# db/seeds.rb

# Crear los productos de paquetes de viaje
Item.find_or_create_by(name: "Paquete viaje pareja") do |i|
    i.description = "4 noches 3 dias Aruba"
    i.price = 399.5
    i.image = "./image/JPG1.jpg"
  end
  
  Item.find_or_create_by(name: "Paquete Viaje Solteros") do |i|
    i.description = "5 noches crucero de verano"
    i.price = 799.5
    i.image = "./image/JPG2.jpg"
  end
  
  Item.find_or_create_by(name: "Paquete viaje amigos") do |i|
    i.description = "6 Noches en las vegas"
    i.price = 999.5
    i.image = "./image/JPG3.jpg"
  end
  
  Item.find_or_create_by(name: "Paquete viaje solo") do |i|
    i.description = "4 Noches en Miami + compras"
    i.price = 699.5
    i.image = "./image/JPG4.jpg"
  end
  
  Item.find_or_create_by(name: "Paquete viaje Hermanos") do |i|
    i.description = "5 dias Orlando"
    i.price = 899.5
    i.image = "./image/JPG5.jpg"
  end
  
  Item.find_or_create_by(name: "Paquete viaje familia") do |i|
    i.description = "6 dias DisneyWorld"
    i.price = 1099.5
    i.image = "./image/JPG6.jpg"
  end
  
  Item.find_or_create_by(name: "paquete viaje con tu mascota") do |i|
    i.description = "5 dias NewYork"
    i.price = 599.5
    i.image = "./image/JPG7.jpg"
  end
  
  # Crear un cliente de ejemplo
  Customer.find_or_create_by(email: "lucasperez@gmail.com") do |c|
    c.first_name = "Lucas"
    c.last_name = "Perez"
  end
  