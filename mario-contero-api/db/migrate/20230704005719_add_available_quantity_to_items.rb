class AddAvailableQuantityToItems < ActiveRecord::Migration[7.0]
  def change
    add_column :items, :available_quantity, :integer, default: 0
  end
end
