# config/routes.rb
Rails.application.routes.draw do
  # Rutas para mostrar la lista de paquetes (productos)
  get "/packages", to: "packages#index"
  
  # Rutas para manejar el carrito de compras
  resources :cart, only: [:index, :create, :destroy]
  post "/cart/checkout", to: "cart#checkout", as: :cart_checkout
  
  # Ruta para mostrar el resumen de la compra y permitir completar la compra
  get "/checkout", to: "cart#checkout"
  
  # Ruta para verificar si un correo electrónico existe antes de realizar la compra
  post "/api/check_email", to: "api#check_email"
  post '/api/create_order', to: 'orders#create'

  # Rutas para manejar los clientes, artículos y órdenes (CRUD)
  resources :customers
  resources :items
  resources :orders, only: [:index]

  # Ruta para mostrar el detalle de una orden específica
  get "/orders/:id", to: "orders#show", as: :order

  namespace :api do
    resources :orders, only: [:create, :show]
  end
end
