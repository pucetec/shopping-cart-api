class Order < ApplicationRecord
    belongs_to :customer
    has_many :line_items
    has_many :items, through: :line_items
  
    def as_json(options = {})
      super(options.merge(include: { line_items: { include: :item } }))
    end
  end