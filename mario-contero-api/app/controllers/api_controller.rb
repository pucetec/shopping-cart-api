# ApiController
class ApiController < ApplicationController
  before_action :set_headers

  def set_headers
    headers['Access-Control-Allow-Origin'] = 'http://localhost:3001'
    headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, PATCH, DELETE, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'
    headers['Access-Control-Allow-Credentials'] = 'true'
  end

  def check_email
    email = params[:email]
    customer = Customer.find_by(email: email)

    if customer
      render json: { exists: true, customer: customer }
    else
      render json: { exists: false }
    end
  end
end