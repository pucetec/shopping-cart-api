import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  mainContainer: {
    display: "flex",
    justifyContent: "space-between",
    padding: "16px",
    flexWrap: "wrap", /* Añadimos flexWrap para que los elementos se ajusten en múltiples filas en dispositivos pequeños */
  },
  leftContainer: {
    flex: "1 1 60%", /* Ajustamos el tamaño del contenedor izquierdo */
    marginBottom: "16px",
  },
  rightContainer: {
    flex: "1 1 35%", /* Ajustamos el tamaño del contenedor derecho */
    backgroundColor: "#f9f9f9",
    padding: "16px",
    borderRadius: "8px",
    boxShadow: "0 2px 4px rgba(0, 0, 0, 0.2)",
    position: "sticky",
    top: "16px",
    maxHeight: "calc(100vh - 32px)",
    overflowY: "auto",
  },
  productCard: {
    display: "flex",
    alignItems: "center",
    marginBottom: "16px",
    padding: "16px",
    backgroundColor: "#f2d7d5",
    borderRadius: "8px",
    transition: "box-shadow 0.2s ease",
    "&:hover": {
      boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
    },
  },
  prodimgContainer: {
    width: "100%",
    height: "40px",
    borderRadius: "8px 8px 0 0",
    overflow: "hidden",
    marginBottom: "16px",
    boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
  },
  prodimg: {
    width: "100%",
    height: "100%",
    objectFit: "cover",
  },
  prodinfo: {
    padding: "16px",
    backgroundColor: "#f9f9f9",
    borderRadius: "0 0 8px 8px",
    boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
  },
  cantidad: {
    backgroundColor: "#007bff",
    color: "#fff",
    padding: "4px 8px",
    borderRadius: "4px",
    marginLeft: "8px",
  },
  formContainer: {
    display: "flex",
    alignItems: "center",
    marginTop: "16px",
  },
  inputEmail: {
    flex: "1 1 70%", /* Ajustamos el tamaño del campo de entrada de correo electrónico */
    marginRight: "16px",
    padding: "8px",
    borderRadius: "4px",
    border: "1px solid #ccc",
  },
  comprarButton: {
    flex: "1 1 30%", /* Ajustamos el tamaño del botón de compra */
    backgroundColor: "#007bff",
    color: "#fff",
    border: "none",
    padding: "8px 16px",
    borderRadius: "4px",
    cursor: "pointer",
  },
  reciboContainer: {
    marginTop: "16px",
    backgroundColor: "#fff",
    borderRadius: "8px",
    padding: "16px",
    boxShadow: "0 2px 4px rgba(0, 0, 0, 0.2)",
  },
  // Estilos para la sección "Productos Reservados"
  reservedContainer: {
    flex: "1 1 100%", /* Ajustamos el tamaño del contenedor de productos reservados */
    position: "sticky",
    top: "16px",
    right: "16px",
    maxHeight: "calc(100vh - 32px)",
    overflowY: "auto",
    marginTop: "16px", /* Añadimos un margen superior */
  },
  reservedProducts: {
    marginBottom: "16px",
  },
  reservedItem: {
    marginBottom: "8px",
    padding: "8px",
    backgroundColor: "#e1e1e1", /* Cambiamos el color de fondo de los productos reservados */
    borderRadius: "8px",
    boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
  },
  reservedItemName: {
    fontWeight: "bold",
  },
  reservedItemQuantity: {
    color: "#666",
  },
  reservedItemPrice: {
    fontWeight: "bold",
  },
});

export default useStyles;
