# app/controllers/api/orders_controller.rb
module Api
    class OrdersController < ApplicationController
      before_action :set_headers
  
      def create
        email = params[:customer]
  
        if valid_email?(email)
          customer = Customer.find_by(email: email)
            
          if customer
            order = Order.new
            order.customer_id = customer.id
            order.description = "Orden Creada"
            order.save
  
            items = params[:items]
  
            items.each do |item|
              found_item = Item.find_by(id: item[:id])
  
              line_item = LineItem.new
              line_item.item_id = found_item.id
              line_item.order_id = order.id
              line_item.quantity = item[:quantity]
              line_item.unit_price = found_item.price
              line_item.save
            end
  
            # Actualizamos el total sumando los precios de todos los LineItem asociados a la orden
            total = order.line_items.sum { |line_item| line_item.unit_price * line_item.quantity }
            order.total = total
            order.save
  
            render json: { id: order.id, message: "Orden creada exitosamente", customer: customer, items: order.line_items, total: total }
          else
            render json: { error: "Correo electrónico no encontrado" }, status: :not_found
          end
        else
          render json: { error: "Correo inválido" }, status: :unprocessable_entity
        end
      end
  
      def show
        order = Order.find(params[:id])
        receipt = generate_receipt(order)
        render json: receipt
      rescue ActiveRecord::RecordNotFound
        render json: { error: 'Orden no encontrada' }, status: :not_found
      end
  
      private
  
      def valid_email?(email)
        email.include?("@")
      end
  
      def generate_receipt(order)
        {
          order_number: order.id,
          customer_email: order.customer.email,
          items: order.line_items.map do |line_item|
            {
              name: line_item.item.name,
              quantity: line_item.quantity,
              unit_price: line_item.item.price
            }
          end,
          total: order.total
        }
      end
  
      def set_headers
        headers['Access-Control-Allow-Origin'] = 'http://localhost:3001'
        headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, PATCH, DELETE, OPTIONS'
        headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'
        headers['Access-Control-Allow-Credentials'] = 'true'
      end
    end
  end
  