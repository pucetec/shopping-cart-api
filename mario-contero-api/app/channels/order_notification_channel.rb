class OrderNotificationChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'order_notification_channel'
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
