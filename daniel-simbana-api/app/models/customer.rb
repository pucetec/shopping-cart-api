class Customer < ApplicationRecord
    has_many :customer_adresses, dependent: :destroy 
    has_many :orders, dependent: :destroy 
end
