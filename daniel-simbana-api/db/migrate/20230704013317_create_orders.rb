class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.integer :customer_id
      t.text :description
      t.integer :total, precision: 10, scale: 2
      t.timestamps
    end
  end
end
