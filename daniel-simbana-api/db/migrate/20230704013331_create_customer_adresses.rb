class CreateCustomerAdresses < ActiveRecord::Migration[7.0]
  def change
    create_table :customer_adresses do |t|
      t.text :street
      t.string :city
      t.timestamps
    end
  end
end
