class CreateLineItems < ActiveRecord::Migration[7.0]
  def change
    create_table :line_items do |t|
      t.integer :item_id
      t.integer :order_id
      t.integer :quantity
      t.integer :unit_price
      t.timestamps
    end
  end
end
