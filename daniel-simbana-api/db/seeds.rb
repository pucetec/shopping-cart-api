
products = Item.find_or_create_by(name:"Water")
products.description = "Tastyless"
products.price = 1
products.image = "https://www.thespruceeats.com/thmb/4Uxr_CKC7aR-UhEicIvVqLaiO0k=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-488636063-5ab2dbd8a8ff48049cfd36e8ad841ae5.jpg"
products.save

products = Item.find_or_create_by(name:"Fruit")
products.description = "Juicy fruit"
products.price = 10
products.image = "https://www.eatingwell.com/thmb/L6xxfuFKUg9MNr4U58-j-UvY_NE=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/bad-fruits-you-should-be-eating-56c1dedd080140b7b34e099943cbb1ec.jpg"
products.save

products = Item.find_or_create_by(name:"Car")
products.description = "Fast car"
products.price = 10000
products.image = "https://images.cars.com/in/v2/stock_photos/7f212472-c429-4681-882b-29e52f4d52b5/884b28bd-d67f-4c90-a7e5-a066090db8f7.png?w=1000"
products.save

products = Item.find_or_create_by(name:"Chair")
products.description = "Confy chair"
products.price = 10
products.image = "https://www.ikea.com/us/en/images/products/lerhamn-chair-black-brown-vittaryd-beige__0728160_pe736117_s5.jpg?f=s"
products.save

products = Item.find_or_create_by(name:"Keyboard")
products.description = "It's AZERTY"
products.price = 113.45
products.image = "https://ergomax.ca/wp-content/uploads/2019/03/kbs225az.png"
products.save

newuser = Customer.find_or_create_by(email:"test@email.com")
newuser.first_name = "Daniel"
newuser.last_name = "Simbana"
newuser.save
