class Order < ApplicationRecord
  belongs_to :customer
  has_many :order_details, dependent: :destroy
  belongs_to :payment_method
end
