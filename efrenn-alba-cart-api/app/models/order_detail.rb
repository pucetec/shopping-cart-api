class OrderDetail < ApplicationRecord
  belongs_to :order
  has_and_belongs_to_many :items, dependent: :destroy
  has_and_belongs_to_many :taxes, dependent: :destroy
end
