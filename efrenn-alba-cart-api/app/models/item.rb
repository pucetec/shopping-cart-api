class Item < ApplicationRecord
  has_and_belongs_to_many :order_details, dependent: :destroy
end
