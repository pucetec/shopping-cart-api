class PaymentMethod < ApplicationRecord
  has_many :orders, dependent: :destroy
end
