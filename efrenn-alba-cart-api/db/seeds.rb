# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

# Create_Items

# i = Item.find_or_create_by(name: 'Dell Latitude Notebook 3250') do |i|
#   i.description = 'Intel Core i9 13th Gen., 32GB RAM, 1TB SSD, Windows 11 Professional'
#   i.available_quantity = 5
#   i.price = 2500
#   i.image = './images/items/notebook_3250.webp'
#   i.is_deleted = false
#   i.save

# end

items = [
  { name: 'Dell Latitude Notebook 3250',
    description: 'Intel Core i9 13th Gen., 32GB RAM, 1TB SSD, Windows 11 Professional', available_quantity: 5, price: 2500, image: './images/items/notebook_3250.webp', is_deleted: false },
  { name: 'Dell Latitude All-In-One 2435',
    description: 'Intel Core i7 12th Gen., 32GB RAM, 450GB SSD, Windows 11 Professional', available_quantity: 5, price: 1200, image: './images/items/aio_computer_2435.webp', is_deleted: false },
  { name: 'Dell Monitor 1945G', description: '1 Display Port, 2 HDMI, Camera Web Integrated, 3 USB 3.0 HUB Ports',
    available_quantity: 5, price: 400, image: './images/items/monitor_1945g.webp', is_deleted: false },
  { name: 'Dell Web Camera 1080FX', description: '1080 Video Recording, Night Mode', available_quantity: 5, price: 150,
    image: './images/items/wc_1080fx.webp', is_deleted: false },
  { name: 'Dell Dock-Station 2H1WC', description: '2 USB 3.0, Smartphone Stand, 10w Wireless Charge',
    available_quantity: 5, price: 200, image: './images/items/ds_2h1wc.webp', is_deleted: false }
]

items.each do |item_params|
  Item.find_or_create_by(item_params)
end

# Create_Payment_Methods
pmethods = [
  { description: 'Transferencia/Depósito', is_deleted: false },
  { description: 'Tarjeta', is_deleted: false }
]

pmethods.each do |pmethod_params|
  PaymentMethod.find_or_create_by(pmethod_params)
end

# Create_Taxes
taxes = [
  { description: 'IVA 12%', amount: 0.12, is_deleted: false },
  { description: 'IVA 0%', amount: 0, is_deleted: false }
]

taxes.each do |tax_params|
  Tax.find_or_create_by(tax_params)
end

# Create_Document_Types
dtypes = [
  { description: 'R.U.C.', is_deleted: false },
  { description: 'Cédula', is_deleted: false },
  { description: 'Pasaporte', is_deleted: false }
]

dtypes.each do |dtype_params|
  DocumentType.find_or_create_by(dtype_params)
end

# Create_Customers
customers = [
  { document_type_id: 2, document_number: '1726616483', firstname: 'Efrenn', lastname: 'Alba', email: 'epalba@puce.edu.ec',
    phone: '0978632798', is_deleted: false },
  { document_type_id: 1, document_number: '1712267770001', firstname: 'Carlos', lastname: 'Manosalvas', email: 'cmanosalvasm@hotmail.com',
    phone: '0982061027', is_deleted: false }
]

customers.each do |customer_params|
  Customer.find_or_create_by(customer_params)
end

# Create_Customers_Addresses
caddresses = [
  { customer_id: 1, first_street: 'Jose Plaza', second_street: 'Gonzalo Pizarro', street_number: 'E2-33',
    city: 'Quito', state: 'Pichincha', is_deleted: false },
  { customer_id: 2, first_street: 'Interoceánica', second_street: 'Manuel Soria', street_number: 'N8-101',
    city: 'Quito', state: 'Pichincha', is_deleted: false }
]

caddresses.each do |caddress_params|
  CustomerAddress.find_or_create_by(caddress_params)
end
