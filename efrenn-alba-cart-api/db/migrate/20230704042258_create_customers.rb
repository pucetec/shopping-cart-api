class CreateCustomers < ActiveRecord::Migration[7.0]
  def change
    create_table :customers do |t|
      t.bigint :document_type_id, null: false
      t.string :document_number, null: false, unique: true
      t.string :firstname, null: false
      t.string :lastname, null: false
      t.string :email, null: false
      t.string :phone, null: false
      t.boolean :is_deleted, null: false, default: false
      t.timestamps
    end
  end
end
