class CreateTaxes < ActiveRecord::Migration[7.0]
  def change
    create_table :taxes do |t|
      t.string :description, null: false
      t.decimal :amount,  precision: 10, scale: 2, default: 0, null: false
      t.boolean :is_deleted, null: false, default: false
      t.timestamps
    end
  end
end
