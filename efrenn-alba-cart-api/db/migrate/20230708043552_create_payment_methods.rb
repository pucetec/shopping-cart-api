class CreatePaymentMethods < ActiveRecord::Migration[7.0]
  def change
    create_table :payment_methods do |t|
      t.string :description, null: false, unique: true
      t.boolean :is_deleted, null: false, default: false
      t.timestamps
    end
  end
end
