class AddForeingKeysToOrders < ActiveRecord::Migration[7.0]
  def change
    add_foreign_key :orders, :customers
    add_foreign_key :orders, :payment_methods
  end
end
