class CreateItems < ActiveRecord::Migration[7.0]
  def change
    create_table :items do |t|
      t.string :name, null: false, unique: true
      t.text :description, null: false
      t.integer :available_quantity, default: 0, null: false
      t.decimal :price, precision: 10, scale: 2, default: 0, null: false
      t.string :image, null: false
      t.boolean :is_deleted, null: false, default: false
      t.timestamps
    end
  end
end
