class CreateOrderDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :order_details do |t|
      t.bigint :order_id, null: false
      t.bigint :item_id, null: false
      t.string :description, null: false
      t.integer :unit_price, null: false
      t.integer :quantity, null: false
      t.decimal :subtotal, precision: 10, scale: 2, default: 0, null: false
      t.bigint :tax_id, null: false
      t.decimal :tax, precision: 10, scale: 2, default: 0, null: false
      t.decimal :total, precision: 10, scale: 2, default: 0, null: false
      t.boolean :is_deleted, null: false, default: false
      t.timestamps
    end
  end
end
