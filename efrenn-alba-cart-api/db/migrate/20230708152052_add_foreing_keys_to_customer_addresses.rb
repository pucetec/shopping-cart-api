class AddForeingKeysToCustomerAddresses < ActiveRecord::Migration[7.0]
  def change
    add_foreign_key :customer_addresses, :customers
  end
end
