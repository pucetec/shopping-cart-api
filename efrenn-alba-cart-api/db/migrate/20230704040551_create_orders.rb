class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.datetime :date_time, null: false
      t.bigint :customer_id, null: false
      t.bigint :payment_method_id, null: false
      t.string :status, null: false
      t.boolean :is_deleted, null: false, default: false
      t.timestamps
    end
  end
end
