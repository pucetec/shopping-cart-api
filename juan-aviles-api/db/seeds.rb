# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
#  Traer Productos
# i=Item.find_or_create_by(name: "Productos") remplaza i.name
#nota en la consola los comandos son  rails db:drop rails db:create rails db:migrate rake db:seed



data = [
  {
    "name": "Process Simulation Design",
    "description": "Design of unit processes for process plant",
    "image": "https://chemicalengineeringworld.com/wp-content/uploads/2021/08/Chemical-Engineering-Software-1024x539.jpg",
    "price": 1099.99
  },
  {
    "name": "Power BI Script Design",
    "description": "Design of scripts for data processing",
    "image": "https://www.freecodecamp.org/news/content/images/2022/08/Python-Power-BI-1.png",
    "price": 1099.99
  },
  {
    "name": "Industrial Document Automation",
    "description": "Automation and standardization of documents",
    "image": "https://blogthinkbig.com/wp-content/uploads/sites/4/2019/12/Word-Excel-New-Icons.jpg?fit=1500%2C1000",
    "price": 3099.99
  },
  {
    "name": "Chemical Product Portfolio",
    "description": "Portfolio of chemical products",
    "image": "https://www.thoughtco.com/thmb/S43kFVYXoxi3gjY6guOrtb6Oe8g=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/chemistry-glassware-56a12a083df78cf772680235.jpg",
    "price": 6899.5
  },
  {
    "name": "Product Formula Design",
    "description": "Development of formulas for products",
    "image": "https://www.thoughtco.com/thmb/S43kFVYXoxi3gjY6guOrtb6Oe8g=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/chemistry-glassware-56a12a083df78cf772680235.jpg",
    "price": 4099.5
  },
  {
    "name": "Product Formula Documentation",
    "description": "Development of documentation for product formulas",
    "image": "https://www.processmaker.com/wp-content/uploads/2021/03/process-documentation.jpg",
    "price": 7099.5
  },
  {
    "name": "Controlled Substance Analysis",
    "description": "Analysis of substances subject to control",
    "image": "https://www.aiju.es/wp-content/uploads/2019/02/fotolia-3245381-subscription-l-1-1024x785.jpg",
    "price": 1599.5
  },
  {
    "name": "Chemical Product Stability Studies",
    "description": "Development of stability studies",
    "image": "https://www.aiju.es/wp-content/uploads/2019/02/fotolia-3245381-subscription-l-1-1024x785.jpg",
    "price": 5099.5
  },
  {
    "name": "Chemical Product Manufacturing",
    "description": "Manufacturing of chemical products",
    "image": "https://www.precisionsg.com/hs-fs/hubfs/chemical%20manufacturing.jpg?width=600&name=chemical%20manufacturing.jpg",
    "price": 4099.5
  },
  {
    "name": "Full Pack for Chemical Products",
    "description": "Complete service for manufacturing of chemical products, factory line movement, development, documentation, and business",
    "image": "https://www.precisionsg.com/hs-fs/hubfs/chemical%20manufacturing.jpg?width=600&name=chemical%20manufacturing.jpg",
    "price": 10099999.5
  }
]

data.each do |item_data|
    Item.find_or_create_by(
      name: item_data[:name],
      description: item_data[:description],
      image: item_data[:image],
      price: item_data[:price]
    )
  end


c1=Customer.new
c1.first_name = "Usuario1"
c1.last_name = "Apellido1"
c1.email= "Usuario1Apellido1@mail.com"
c1.save
c2=Customer.new
c2.first_name = "Comprador"
c2.last_name = "Compulsivo"
c2.email= "CCompulsivo@mail.com"
c2.save
c3=Customer.new
c3.first_name = "CompradorPaborrar"
c3.last_name = "Compulsivo"
c3.email= "CCompulsivo2@mail.com"
c3.save

cad=CustomerAddress.new
cad.customer_id=c1.id
cad.street = "POr alla1"
cad.city = "Quitoff"
cad.save
cad=CustomerAddress.new
cad.customer_id=c2.id
cad.street = "Lejos2"
cad.city = "Quitoff"
cad.save
cad=CustomerAddress.new
cad.customer_id=c3.id
cad.street = "Lejos3"
cad.city = "Manso Guayashh"
cad.save