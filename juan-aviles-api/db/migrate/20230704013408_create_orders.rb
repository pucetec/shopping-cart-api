class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.integer :customer_id
      t.decimal :description, precision: 10, scale:2
      t.decimal :price, precision: 10, scale:2

      t.timestamps
    end
  end
end
