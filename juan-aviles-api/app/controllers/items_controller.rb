class ItemsController < ApplicationController
   before_action :set_headers
    def index
        render json: Item.all
    end
end
