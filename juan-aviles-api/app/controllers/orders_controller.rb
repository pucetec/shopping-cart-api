class OrdersController < ApplicationController
  before_action :set_headers

  def index
    render json: Order.all
  end

  def create
    
    created_order = false

    #binding.pry
    email = params[:payload][:customer]
    
    if valid_email?(email)
      customer = Customer.find_by(email: email)

      if customer
        order = Order.new
        order.customer_id = customer.id
        order.description = "Our first order"
        order.save

        items = params[:payload][:items]
        total = 0
        items.each do |item|
          found_item = Item.find(item["id"])
          if found_item

            line_item = LineItem.new
            line_item.item_id = found_item.id
            line_item.order_id =  order.id
            line_item.quantity = item["quantity"]
            line_item.unit_price = found_item.price
            
            line_item.save
            total = total + line_item.quantity * found_item.price
          
          end
        end
        order.price = total
        order.save
        created_order = true

      end
    
    end
    
    if created_order 
      render json: { message: "Successful", order: order, customer: customer, items: items } 
    else
      render json: { message: "Please try again or Email not found in the database"  }, status: :not_found
    end

  end

  private

  def valid_email?(email)
    # Validate email format using a regular expression or an email validation gem if necessary.
    # For example:
    /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i.match?(email)
  end

end
