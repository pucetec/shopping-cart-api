Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  #  atar al controlador
  # get "/products", to: ""
  resources :items
  resources :customers
  resources :orders
  

end
