Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get "/products", to: "items#index"
  #get "/customer_addresses", to: "customer_addresses#index"

  resources :customers
  resources :customer_addresses
  resources :orders
end
