class OrdersController < ApplicationController
    before_action :set_headers
  
    def create
      customer_email = params[:customer_email]
      items = params[:items]
  
      # Validate if required parameters are present
      if customer_email.blank? || items.blank?
        render json: { message: "Datos de la compra incompletos" }, status: :unprocessable_entity
        return
      end
  
      customer = Customer.find_by(email: customer_email)
  
      if customer.nil?
        render json: { message: "Usuario no existe" }, status: :unprocessable_entity
        return
      end
  
      order = Order.new
      order.customer_id = customer.id
      order.description = "Primera Orden"
      order.save
  
      total = 0
      items.each do |item|
        #binding.pry
        found_item = Item.find(item["idItem"])
        if found_item
          line_item = LineItem.new
          line_item.item_id = found_item.id
          line_item.order_id = order.id
          line_item.quantity = item["quantityItem"]
          line_item.unit_price = found_item.price
          line_item.save
          total += item["quantityItem"] * found_item.price
        end
      end
  
      order.total = total
      if order.save
        render json: { message: "Compra Exitosa", order: order }
      else
        render json: { message: "Compra Rechazada" }, status: :unprocessable_entity
      end
    end
  end
  