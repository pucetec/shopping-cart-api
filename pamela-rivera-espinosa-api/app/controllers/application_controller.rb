class ApplicationController < ActionController::API
    def set_headers
        headers['Access-Control-Allow-Origin'] = "http://localhost:3000"
        headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD'
        headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'    
    end
end
