class CreateCustomerAddresses < ActiveRecord::Migration[7.0]
  def change
    create_table :customer_addresses do |t|
      #t.integer :customer_id
      t.references :Customer 
      t.text :street
      t.text :city
      t.timestamps
    end
  end
end
