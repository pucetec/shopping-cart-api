# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

i = Item.find_or_create_by(name: "CRAYOLAS")
i.description = "Caja de 12 colores"
i.price = 5.99
i.image = "./../image/crayolas.png"
i.save

i = Item.find_or_create_by(name: "CORRECTORES")
i.description = "Cinta Correctora de 6MM"
i.price = 2.55
i.image = "./../image/correctores.png"
i.save

i = Item.find_or_create_by(name: "BANDERITAS ADHESIVAS")
i.description = "Paquete de 5 colores multiusos"
i.price = 1.33
i.image = "./../image/banderitas.png"
i.save

i = Item.find_or_create_by(name: "FOLDERS")
i.description = "De cartulina en varios colores"
i.price = 1.99
i.image = "./../image/folders.png"
i.save

c = Customer.find_or_create_by(first_name: "Juan Pablo")
c.last_name = "Vargas"
c.email = "jpvargas@mail.com"
c.save

c = Customer.find_or_create_by(first_name: "Simon")
c.last_name = "Villamil"
c.email = "svillamil@mail.com"
c.save

c = Customer.find_or_create_by(first_name: "Pablo")
c.last_name = "Rivera"
c.email = "privera@mail.com"
c.save

c = Customer.find_or_create_by(first_name: "Martin")
c.last_name = "Isaza"
c.email = "misaza@mail.com"
c.save