
items = [
    { name: 'SUDADERA ONE PIECE', description: 'Sudadera licencia One Piece con capucha, print en contraste y de manga larga.', price: 29.99, image: 'https://static.pullandbear.net/2/photos//2023/V/0/2/p/8593/523/800/8593523800_2_6_8.jpg?t=1677179609535&imwidth=750' },
    { name: 'SUDADERA CREMALLERA', description: 'Sudadera con cierre de cremallera, capucha con cordones ajustables, bolsillo tipo canguro, detalle de rib y etiqueta en el bajo.', price: 25.99, image: 'https://static.pullandbear.net/2/photos//2023/V/0/2/p/4596/573/803/4596573803_2_6_8.jpg?t=1677170334380&imwidth=750' },
    { name: 'SUDADERA BÁSICA', description: 'Sudadera básica con capucha, de manga larga, con bolsillo delantero tipo canguro, detalle de etiqueta en el bajo y confeccionada en mezcla de algodón.', price: 40.99, image: 'https://static.pullandbear.net/2/photos//2023/V/0/2/p/8591/513/404/8591513404_2_6_8.jpg?t=1677177227908&imwidth=750' },
    { name: 'SUDADERA TUPAC', description: 'Sudadera licencia Tupac en color beis con print en contraste, capucha con cordón, bolsillo canguro y de manga larga.', price: 19.99, image: 'https://static.pullandbear.net/2/photos//2023/V/0/2/p/8593/521/251/8593521251_2_6_8.jpg?t=1677171290062&imwidth=750' },
    { name: 'SUDADERA NARUTO', description: 'Sudadera marrón licencia Naruto con capucha con cordón, bolsillo canguro y de manga larga.', price: 25.99, image: 'https://static.pullandbear.net/2/photos//2023/V/0/2/p/8597/525/717/8597525717_2_6_8.jpg?t=1677171360962&imwidth=750' }
  ]
  
  items.each do |item_params|
    Item.find_or_create_by(item_params)
  end
  
  customers = [
    { first_name: 'John', last_name: 'Doe', email: 'johndoe@example.com' },
    { first_name: 'Jane', last_name: 'Smith', email: 'janesmith@example.com' },
    { first_name: 'Alice', last_name: 'Johnson', email: 'alicejohnson@example.com' },
    { first_name: 'Bob', last_name: 'Williams', email: 'bobwilliams@example.com' },
    { first_name: 'Emma', last_name: 'Brown', email: 'emmabrown@example.com' },
    { first_name: 'Joel', last_name: 'Layedra', email: 'layedra75@gmail.com' },
  ]
  
  customers.each do |customer_params|
    Customer.find_or_create_by(customer_params)
  end
  
  customer_addresses = [
    { customer_id: 1, street: '123 Main St', city: 'Example City' },
    { customer_id: 2, street: '456 Elm St', city: 'Another City' },
    { customer_id: 3, street: '789 Oak St', city: 'Yet Another City' },
    { customer_id: 4, street: '321 Pine St', city: 'Some City' },
    { customer_id: 5, street: '654 Maple St', city: 'Different City' }
  ]
  
  customer_addresses.each do |customer_address_params|
    CustomerAddress.find_or_create_by(customer_address_params)
  end
  
  line_items = [
    { item_id: 1, order_id: 1, quantity: 2, unit_price: 10 },
    { item_id: 2, order_id: 1, quantity: 1, unit_price: 15 },
    { item_id: 3, order_id: 2, quantity: 3, unit_price: 20 },
    { item_id: 4, order_id: 2, quantity: 2, unit_price: 12 },
    { item_id: 5, order_id: 3, quantity: 1, unit_price: 18 }
  ]
  
  line_items.each do |line_item_params|
    LineItem.find_or_create_by(line_item_params)
  end
  
  orders = [
    { customer_id: 1, description: 'Order 1', total: 50 },
    { customer_id: 2, description: 'Order 2', total: 75 },
    { customer_id: 3, description: 'Order 3', total: 100 },
    { customer_id: 4, description: 'Order 4', total: 60 },
    { customer_id: 5, description: 'Order 5', total: 90 }
  ]
  
  orders.each do |order_params|
    Order.find_or_create_by(order_params)
  end
