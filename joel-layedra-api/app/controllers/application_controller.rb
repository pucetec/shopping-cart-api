class ApplicationController < ActionController::Base
    def set_headers
        headers['Access-Control-Allow-Origin'] = "http://localhost:3000"
    end
end
