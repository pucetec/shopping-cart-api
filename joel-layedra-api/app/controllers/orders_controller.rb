class OrdersController < ApplicationController
  def create
    customer = Customer.find_by(email: params[:email])

    unless customer
      render json: { error: 'No se puede efectuar la compra ya que el correo electrónico no está registrado.' }, status: :unprocessable_entity
      return
    end

    total = 0
    params[:purchaseDetails].each do |item|
      total += item[:quantity].to_i * item[:price].to_f
    end

    order = customer.orders.create(description: 'Compra en línea', total: total)
    if order.persisted?
      line_items = params[:purchaseDetails].map do |item|
        { item_id: item[:id], quantity: item[:quantity], unit_price: item[:price] }
      end
      order.line_items.create(line_items)
      render json: { order_id: order.id, customer_id: customer.id }, status: :created
    else
      render json: { error: 'Ocurrió un error al crear la orden.' }, status: :unprocessable_entity
    end
  end
end
