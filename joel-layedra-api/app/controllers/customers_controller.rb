class CustomersController < ApplicationController

    def create
      email = params[:email]
      customer = Customer.find_by(email: email)
      render json: { exists: customer.present? }
    end
  end
  