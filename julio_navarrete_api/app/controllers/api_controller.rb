class ApiController < ActionController::API
    before_action :set_headers
    def set_headers
        headers['Access-Control-Allow-Origin'] = 'http://localhost:3000'
    end
end