class CustomersController < ApiController
    def index
        render json: Customer.all
    end
    def login
        email = params[:email]
        customer = Customer.find_by(email: email)
        if customer
            render json: { message: 'Cliente Encontrado', customer: customer }, status: :ok
        else
            render json: { error: 'El cliente no existe' }, status: :not_found
        end
    end
end
