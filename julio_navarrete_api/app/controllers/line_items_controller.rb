class LineItemsController < ApiController
    def index
        render json: LineItem.all
    end
    def create
        # Parsear los datos JSON recibidos en el cuerpo de la solicitud
        data = JSON.parse(request.body.read, object_class: OpenStruct)
        
        # Utilizar los datos para extraer el id del cliente
        customerData = Customer.find_by(email: data.customer)
        
         # Crear el objeto Order con los datos necesarios
        order = Order.new(
            customer_id: customerData.id,
            description: customerData.first_name + ' ' + customerData.last_name,
        )
        # Guardar el objeto Order en la base de datos
        if order.save
            # Si el objeto Order se guardó correctamente, continuar con la creación de LineItems
            total = 0
            item_collection = data.items
            item_collection.each do |item|
                LineItem.find_or_create_by(order_id: order.id, item_id: item.id) do |li|
                    li.quantity = item.quantity
                    li.unit_price = item.price
                end
                total = total + (item.quantity.to_i * item.price.to_f)
            end
            order.total = total
            order.save

            # Finalmente, devolver una respuesta adecuada según el resultado de la operación
            render json: { message: 'Pedido creado exitosamente, (Orden #: ' + order.id.to_s + ').', order: order }, status: :created
        else
            order.destroy if order
            # Si ocurrió algún error al guardar el objeto Order, devolver una respuesta de error
            render json: { error: 'Error al crear el pedido' }, status: :unprocessable_entity
        end
    end
end
