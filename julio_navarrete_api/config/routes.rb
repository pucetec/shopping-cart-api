Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  # get "/products", to: "products#index"
  resources :products
  resources :line_items, only: [:create, :index, :update, :destroy]
  resources :customers, only: [:create, :index, :update, :destroy] do
    post "/login", to: "customers#login", on: :collection
  end  
end
