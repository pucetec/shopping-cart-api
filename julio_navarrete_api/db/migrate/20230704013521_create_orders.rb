class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.references :customer, foreign_key: true
      t.text :description
      t.decimal :total, precision:10, scale: 2
      t.timestamps
    end
    
  end
end
