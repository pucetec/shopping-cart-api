class CreateCustomerAddresses < ActiveRecord::Migration[7.0]
  def change
    create_table :customer_addresses do |t|
      t.references :customer, foreign_key: true
      t.text :street
      t.text :city
      t.timestamps
    end
  end
end
