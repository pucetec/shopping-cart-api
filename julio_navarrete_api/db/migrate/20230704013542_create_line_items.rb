class CreateLineItems < ActiveRecord::Migration[7.0]
  def change
    create_table :line_items do |t|
      t.references :order, foreign_key: true
      t.references :item, foreign_key: true
      t.integer :quantity
      t.decimal :unit_price, precision:10, scale: 2
      t.timestamps
    end
    
  end
end
