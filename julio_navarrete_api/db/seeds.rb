# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
item_collection=[
    { name:'Samsung A30', description:'Gama ALta 8Gb Ram', price:500.30, image:'https://guayaquil.solutekla.com/photo/1/samsung/celulares/celular_samsung_galaxy_a30_azul_pantalla_64_infinityu_full_hd_color_super_amoled_cmara_principal_5_mpx_16mpx_cmara_frontal_16_mp_ram_4gb_memoria_64_gb_expandible_hasta_512_gb_red_4g_lte__octacore_18ghz_16_ghz_android_90_pie_batera_4000mahreconocimiento_facial_escaner_de_huellas__dimensiones_1585_x_747_x_77_mm_peso_166gr_diseo_impresionante_pantalla_infinita_asistente_personal_bixby_garanta_un_ao/celular_samsung_galaxy_a30_azul_pantalla_64_infinityu_full_hd_color_super_amoled_cmara_principal_5_mpx_16mpx_cmara_frontal_16_mp_ram_4gb_memoria_64_gb_expandible_hasta_512_gb_red_4g_lte__octacore_18ghz_16_ghz_android_90_pie_batera_4000mahreconocimiento_facial_escaner_de_huellas__dimensiones_1585_x_747_x_77_mm_peso_166gr_diseo_impresionante_pantalla_infinita_asistente_personal_bixby_garanta_un_ao_0001' },
    { name:'Televisión Android', description:'Con Android 65"', price:500.30, image:'https://www.lg.com/ec/images/tvs/md07551606/gallery/D-1.jpg' },
    { name:'Celular Flit', description:'Gama ALta 16Gb Ram', price:900.30, image:'https://i.blogs.es/74ffb7/samsung-galaxy-z-flip-00/450_1000.jpg' }
]

item_collection.each do |item|
    Item.find_or_create_by(item.slice(:name)) do |c|
        c.description = item[:description]
        c.price = item[:price]
        c.image = item[:image]
    end
end


customer_attributes = [
    { first_name: 'Javier', last_name: 'Flores', email: 'icesasoftcorp@gmail.com' },
    { first_name: 'Julio', last_name: 'Navarrete', email: 'jnavarrete@softbuilders.com' }
  ]
  
customer_attributes.each do |attributes|
    customer=Customer.find_or_create_by(attributes) do |c|
        c.email = attributes[:email]
    end

    customer_addresses_attributes = [
        { customer_id: customer.id, street: 'Gonzalo Perez', city: 'Quito' },
        { customer_id: customer.id, street: 'Barcelona Street', city: 'España' }
    ]
    
    customer_addresses_attributes.each do |address_attributes|
        CustomerAddress.find_or_create_by(address_attributes.slice(:customer_id, :street)) do |a|
            a.city = address_attributes[:city]
        end
    end
end